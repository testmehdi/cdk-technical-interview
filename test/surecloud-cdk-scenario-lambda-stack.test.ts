import * as cdk from 'aws-cdk-lib';
import { Template } from 'aws-cdk-lib/assertions';
import { SurecloudCdkScenarioLambdaStack } from '../lib/surecloud-cdk-scenario-lambda-stack';

test('Lambda has correct properties', () => {
    const app = new cdk.App();
    const stack = new SurecloudCdkScenarioLambdaStack(app, 'surecloud-cdk-scenario-prerequisite-stack');

    const template = Template.fromStack(stack);

    template.hasResourceProperties("AWS::Lambda::Function", {
      Handler: "index.handler",
      Runtime: "python3.7",
});
})