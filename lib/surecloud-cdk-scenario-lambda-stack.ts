import * as cdk from 'aws-cdk-lib';
import * as lambda from 'aws-cdk-lib/aws-lambda';
import * as path from 'path';
import * as iam from 'aws-cdk-lib/aws-iam';

export class SurecloudCdkScenarioLambdaStack extends cdk.Stack {
    constructor(scope: cdk.App, id: string, props?: cdk.StackProps) {
        super(scope, id, props);

        const MyFunction = new lambda.Function(this, 'MyFunction', {
            memorySize: 1024,
            timeout: cdk.Duration.seconds(5),
            runtime: lambda.Runtime.PYTHON_3_7,
            handler: 'index.handler',
            code: lambda.Code.fromAsset(path.join(__dirname, 'index.py.zip')),
        })

        const EC2Perms = new iam.PolicyDocument({
            statements: [
                new iam.PolicyStatement({
                    resources: ['arn:aws:lambda:*'],
                    actions: ['EC2:*'],
                    effect: iam.Effect.ALLOW,
                }),
            ],
    });
}
}



