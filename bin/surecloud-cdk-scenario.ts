#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import {SurecloudCdkScenarioPrerequisiteStack} from '../lib/surecloud-cdk-scenario-prerequisite-stack';
import {SurecloudCdkScenarioLambdaStack} from '../lib/surecloud-cdk-scenario-lambda-stack';

const app = new cdk.App();
new SurecloudCdkScenarioPrerequisiteStack(app, 'surecloud-cdk-scenario-prerequisite-stack');
new SurecloudCdkScenarioLambdaStack(app, 'surecloud-cdk-scenario-lambda-stack');

