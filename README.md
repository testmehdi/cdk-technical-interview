# SureCloud AWS CDK Technical interview

I have created a CDK stack called `surecloud-cdk-scenario-lambda-stack.ts`. This creates a Lambda function with a Python runtime. The code for the function is contained in the index.py.zip file. When this code is executed, it lists all running EC2 instances along with all associated tags.

I have also created a tests file which confirms that the Lambda function has all the expected properties.

Both stacks can be run by executing the script.sh file - this will firstly run the tests (and exit if any of them fail), before deploying the prequestite stack, and finally deploying the Lambda stack, where the output of the instance names and tags can be seen in the AWS console.
